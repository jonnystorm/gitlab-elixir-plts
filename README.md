# Dialyzer PLTs for Elixir on GitLab CI

**New PLTs generated daily**

This project is a blatant clone of Daniel Berkompas's
[original project for Travis CI](
  https://github.com/danielberkompas/travis_elixir_plts
). I take no credit for the idea or implementation, but any
errors are solely my own.

Many thanks to Daniel for teaching me something about the
guts of Travis CI and AWS S3 integration.

The file format is
`elixir-#{elixir_version}_#{otp_version}.plt`.

See this project's `.gitlab-ci.yml` to learn how the PLTs
are generated and pushed to the repository. Scheduling is
handled by the GitLab CI job scheduler.
