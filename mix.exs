defmodule GitLabPLTGenerator.Mixfile do
  use Mix.Project

  def project do
    [ app: :gitlab_plt_generator,
      version: "0.0.2",
      elixir: "~> 1.6",
      name: "GitLab PLT Generator",
      source_url: "https://gitlab.com/jonnystorm/gitlab-elixir-plt-generator",
      dialyzer: [
        plt_file: plt_filename(),
      ],
      deps: deps(),
    ]
  end

  def application do
    [applications: [:logger]]
  end

  defp deps do
    []
  end

  defp plt_filename do
    otp_release =
      System.get_env("OTP_VERSION") ||
        :erlang.system_info(:otp_release)

    "elixir-#{System.version}_#{otp_release}.plt"
  end
end

